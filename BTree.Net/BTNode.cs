﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace BTree.Net
{
    public class BTNode<T>
    {
        public T Value { get; set; } = default(T);
        public BTNode<T> Left { get; set; }
        public BTNode<T> Right { get; set; }
        
        public BTNode()
        {
        }

        public BTNode(T value)
        {
            Value = value;
        }

        public BTNode(T value, BTNode<T> left = null, BTNode<T> right = null)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public int GetHeight()
        {
            return GetHeightRecursive(this) - 1;
        }

        public int GetDepth(BTNode<T> node)
        {
            if (node == null)
                return 0;

            int depth = 0;
            var ret = GetDepthRecursive(node, ref depth);
            return depth;
        }

        private bool GetDepthRecursive(BTNode<T> node, ref int depth)
        {
            if (node == null)
                return false;

            if(Comparer<T>.Default.Compare(node.Value, Value) == 0)
                return true;

            depth++;

            if (node.Left != null && GetDepthRecursive(node.Left, ref depth) == true)
                return true;
            if (node.Right != null && GetDepthRecursive(node.Right, ref depth) == true)
                return true;

            return false;
        }

        private int GetHeightRecursive(BTNode<T> node)
        {
            if (node == null)
                return 0;

            return Math.Max(GetHeightRecursive(node.Right), GetHeightRecursive(node.Left)) + 1;
        }

    }


}
