﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTree.Net
{
    public class SortedBTree<T>
    {
        public BTNode<T> Root { get; private set; } = null;
        public int Depth { get; set; }

        public SortedBTree()
        {
        }

        public virtual void Clear()
        {
            Root = null;
        }

        public bool Insert(T value)
        {
            if (Root == null)
            {
                Root = new BTNode<T>(value);
                return true;
            }
            else
                return InsertNode(Root, value);
        }

        private bool InsertNode(BTNode<T> node, T value)
        {
            try
            {
                if (Comparer<T>.Default.Compare(node.Value, value) < 0)
                {
                    if (node.Right == null)
                    {
                        node.Right = new BTNode<T>(value);
                        return true;
                    }
                    else
                        return InsertNode(node.Right, value);
                }
                else
                {
                    if (node.Left == null)
                    {
                        node.Left = new BTNode<T>(value);
                        return true;
                    }
                    else
                        return InsertNode(node.Left, value);
                }
            }
            catch(Exception exp)
            {
                throw exp;
            }
        }

        public int GetMaxDepth()
        {            
            return GetMaxDepthRecursive(Root) - 1;
        }

        public int GetDepth(T value)
        {            
            int depth = 0;
            var ret = GetDepthRecursive(Root, value, ref depth);
            return depth;
        }

        public void Print()
        {
            Root.Print();
        }
        
        private int GetMaxDepthRecursive(BTNode<T> root)
        {
            if (root == null)
                return 0;

            return Math.Max(GetMaxDepthRecursive(root.Right), GetMaxDepthRecursive(root.Left)) + 1;
        }
        
        private bool GetDepthRecursive(BTNode<T> node, T value, ref int depth)
        {
            if (node == null)
            {
                depth--;
                return false;
            }

            if (Comparer<T>.Default.Compare(node.Value, value) == 0)
                return true;

            depth++;

            if (node.Left != null && GetDepthRecursive(node.Left, value, ref depth) == true)
                return true;
            if (node.Right != null && GetDepthRecursive(node.Right, value, ref depth) == true)
                return true;

            depth--;
            return false;
        }
    }
}
