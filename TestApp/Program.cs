﻿using BTree.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedBTree<object> bTree = new SortedBTree<object>();
            /*
            bTree.Insert("ttttt");
            bTree.Insert("qqqqq");
            bTree.Insert("g");
            bTree.Insert("hhdfg");
            bTree.Insert("weiuryxc");
            bTree.Insert("aaaa");
            bTree.Insert("bbb");
            bTree.Insert("bbbb");
            bTree.Insert("ccc");
            bTree.Insert("ddd");
            bTree.Insert("dcdcdc");
            bTree.Insert("cdcdcd");
            //*/
            //*
            bTree.Insert(61);
            bTree.Insert(12);
            bTree.Insert(113);
            bTree.Insert(11);
            bTree.Insert(310);
            bTree.Insert(91);
            bTree.Insert(131);
            bTree.Insert(128);
            bTree.Insert(181);

            bTree.Insert(16);
            bTree.Insert(22);
            bTree.Insert(43);
            bTree.Insert(1);
            bTree.Insert(310);
            bTree.Insert(4);
            bTree.Insert(35);
            bTree.Insert(17);
            //*/
            bTree.Print();

            Console.WriteLine("Root Height :" + bTree.Root.GetHeight() + " [" + bTree.Root.Value + "]");
            Console.WriteLine("Node Height :" + bTree.Root.Left.Left.GetHeight() + " [" + bTree.Root.Left.Left.Value + "]");

            Console.WriteLine("Root Depth :" + bTree.Root.GetDepth(bTree.Root) + " [" + bTree.Root.Value + "]");
            Console.WriteLine("Node Depth :" + bTree.Root.Left.Left.GetDepth(bTree.Root) + " [" + bTree.Root.Left.Left.Value + "]");

            Console.WriteLine("128 Depth :" + bTree.GetDepth(128));
            Console.WriteLine("16 Depth :" + bTree.GetDepth(16));

            Console.WriteLine("Max Depth :" + bTree.GetMaxDepth());

        }
    }
}
