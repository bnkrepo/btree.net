A simple Binary Search Tree implementation in C#. 

The usage is as simple as shown below:


namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedBTree<int> bTree = new SortedBTree<int>();

            bTree.Insert(16);
            bTree.Insert(22);
            bTree.Insert(43);
            bTree.Insert(1);
            bTree.Insert(310);
            bTree.Insert(4);
            bTree.Insert(35);
            bTree.Insert(17);

            /*
            SortedBTree<string> bTree = new SortedBTree<string>();
            
            bTree.Insert("ttttt");
            bTree.Insert("qqqqq");
            bTree.Insert("g");
            bTree.Insert("hhdfg");
            bTree.Insert("weiuryxc");
            bTree.Insert("aaaa");
            bTree.Insert("bbb");
            bTree.Insert("bbbb");
            bTree.Insert("ccc");
            bTree.Insert("ddd");
            bTree.Insert("dcdcdc");
            bTree.Insert("cdcdcd");
            //*/
            
            bTree.Print();
        }
    }
}

SortedBTree is a generic class so works with string, int etc. types. 
